<%--
  Created by IntelliJ IDEA.
  User: azedine
  Date: 09/10/2020
  Time: 14:38
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Calculate page </title>

    <style type="text/css">
        .error {
            color: red;
            position: fixed;
            text-align: left;
        }
    </style>
</head>
<body>

<h1 align="center"> LCalculator </h1>
<hr/>


<%--@elvariable id="user" type="domain.InfoUser"--%>
<form:form action="process-homepage" modelAttribute="user" method="post">
    <div align="center">
        <p>
            <label for="yn">Your Name :</label>
            <form:input id="yn" path="yourname"/>
            <form:errors path="yourname" cssClass="error"/>
        </p>
        <p>
            <label for="cn">Crush Name :</label>
            <form:input id="cn" path="crushname"/>
            <form:errors path="crushname" cssClass="error"/>
        </p>
        <p>
            <label for="ck">ckeck user condition </label>
            <form:checkbox id="ck" path="usecondition"/>
            <form:errors path="usecondition" cssClass="error"/>
        </p>
        <input type="submit" value="calculate">
    </div>

</form:form>

</body>
</html>
