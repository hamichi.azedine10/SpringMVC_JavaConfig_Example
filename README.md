1 - Spring mvc data binding  -  09/10/2020 14h30 \
2 - Spring mvc data binding using Data transfer Object  DTO - 15/10/2020 17h30\
3 - Spring MVC data binding  using spring form and DTO to loed default data from DTO (path input must match with DTO propertie) - 20/10/2020 17h15 \
4 - Utiliser ModelAttribute a la place du Model  26/10/2020\
5 - Spring mvc Validation -validation des donnée coté serveur \
6 - validation des objet embarqués \
7 - Formatter : formattage des données entrantes sous forme chaine de carcteres a un objet  (methode parse) et vis versa (methode print ) \
8 - Annotations personnalisées 10/12/2020  \
9 - ajouter  Init Binder | Property Editor | Converter | Validator with Real-Time Scenario 19/12/2020 14h24\
10 - Utiliser les Cookies pour tracer un user  connecté   03/01/2021 19h13 \
   10-a lire et ecrire le coockie  en utilisant  httpservlet (request et response ) 04/01/2021 13h59\
   10-b lire le coockie  en utilisant  @coockievalue de spring  04/01/2021 13h05\
11 - identifier les users en utilisant les session à la place des Cookies  05/01/2021 11h19\
    11-a utiliser httpsession\
    11-b Session avec spring annotation: @SessionAttributes/@SessionAttribute, il ne faut pas utiliser l'annotation @ModelAttribute (il faur creer le model manuellement)\
12 - Envoie  de mail a un compte existant 11/01/2021\
13 - Utiliser l'objet Environnement pour recuperer les properties du mailsender  a paartir d'un fichier email.properties 
14 - valider les données d'un simple model  quand on ne peut pas creer de @ModelAttribute 
