<%--
  Created by IntelliJ IDEA.
  User: azedine
  Date: 02/01/2021
  Time: 16:33
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>sendemail-page</title>
    <style type="text/css">
        .error {
            color: red;
            position: fixed;
            text-align: left;
        }
    </style>
</head>
<body>
<h3> Hi ${username} , send result to your Email </h3>
<br/>
<%--@elvariable id="EmailUserDTO" type="domain.EmailUser"--%>
<form:form action="sendEmail" modelAttribute="EmailUserDTO" method="post">
    <h3>
        insert your Email : <form:input path="email"/>
        <form:errors path="email" cssClass="error"/>
    </h3>
    <h3><input type="submit" value="Send"></h3>

</form:form>

</body>
</html>
