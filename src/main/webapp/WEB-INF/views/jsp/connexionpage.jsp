<%--
  Created by IntelliJ IDEA.
  User: azedine
  Date: 05/11/2020
  Time: 16:08
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Connection page </title>
    <style type="text/css">
        .error {
            color: red;
            position: fixed;
            text-align: left;
        }
    </style>
</head>
<body>
<h1 align="center"> LCalculator </h1>
<p align="right"><a href="/SpringMVC/SpringMVC_JavaConfig_Example/SMVCExample/register/registrationpage"> Register </a></p>

<%--@elvariable id="connexioninfo" type="domain.Connectioninfo"--%>
<form:form action="Singin" modelAttribute="connexioninfo" method="post">
    <div>
        <table align="center">
            <tr>
                <td><label for="nm"> Name :</label></td>
                <td><form:input id="nm" path="name"/>
                    <form:errors path="name" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td><label for="pswd"> Password :</label></td>
                <td>
                    <form:password id="pswd" path="password"/>
                    <form:errors path="password" cssClass="error"/>
                </td>
            </tr>
        </table>
    </div>
    <br/>
    <div align="center">
        <input type="submit" value="connecter">
    </div>
</form:form>
</body>
</html>
