<%--
  Created by IntelliJ IDEA.
  User: azedine
  Date: 26/10/2020
  Time: 12:36
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Registrtion</title>
    <style type="text/css">
        .error {
            color: red;
            position: fixed;
            text-align: left;
        }
    </style>
</head>
<body>
<h1 align="center"> LCalculator </h1>
<p align="right"><a href="/SpringMVC/SpringMVC_JavaConfig_Example/SMVCExample/Singin"> Sign in </a></p>

<%--@elvariable id="RegisterUserInfo" type="domain"--%>
<form:form action="RegistrationUser" modelAttribute="RegisterUserInfo" method="post">
    <div>
        <table align="center">
            <tr>
                <td><label for="nm"> Name :</label></td>
                <td>
                    <form:input id="nm" path="name"/>
                    <form:errors path="name" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td><label for="usernm"> User Name :</label></td>
                <td>
                    <form:input id="usernm" path="username"/>
                    <form:errors path="username" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td><label for="pswd"> Password :</label></td>
                <td>
                    <form:password id="pswd" path="password"/>
                    <form:errors path="password" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td><label for="ag"> Age :</label></td>
                <td>
                    <form:input id="ag" path="age"/>
                    <form:errors path="age" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td><label for="ctry"> Country :</label></td>
                <td>
                    <form:select id="ctry" path="country" items="${liste}"/>
                </td>
            </tr>
            <tr>
                <td><label for="hobby"> Hobbies : </label></td>
                <td>
                    Football <form:checkbox id="hobby" path="hobbies" value="Football"/>
                    Tennis<form:checkbox id="hobby" path="hobbies" value="Tennis"/>
                    Handball <form:checkbox id="hobby" path="hobbies" value="Handball"/>
                    Natation <form:checkbox id="hobby" path="hobbies" value="Natation"/>
                </td>
            </tr>
            <tr>
                <td><label for="gender"> Gender : </label></td>
                <td>
                    Male <form:radiobutton id="gender " path="Gender" value="Male"/>
                    Female <form:radiobutton id="gender" path="Gender" value="Female"/>
                </td>
            </tr>
            <tr>
                <td><label for="eml">Email </label></td>
                <td>
                    <form:input id="eml" path="communication.email"/>
                    <form:errors id="eml" path="communication.email" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td><label for="eml">Phone </label></td>
                <td>
                    <form:input id="phn" path="communication.phone"/>
                    <form:errors id="phn" path="communication.phone.coodepays" cssClass="error"/>
                    <form:errors id="phn" path="communication.phone.numero" cssClass="error"/>
                </td>
            </tr>
        </table>
    </div>
    <br/>
    <div align="center">
        <input type="submit" value="Register">
    </div>
</form:form>
</body>
</html>
