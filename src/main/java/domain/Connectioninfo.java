package domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Connectioninfo {
    @NotBlank(message = "* Le champs Name ne doit pas etre vide ")
    @Size(min = 3, max = 18, message = "* le nome doit etre entre 3 et 18 caracteres ")
    private String name;
    @NotBlank(message = "* Le  champs password  est obligatoire ")
    @Size(min = 5, message = "* le mot de passe doit contenir plus de 5 caracteres ")
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Connectioninfo{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
