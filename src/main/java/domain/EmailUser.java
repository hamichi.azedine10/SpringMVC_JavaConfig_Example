package domain;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class EmailUser {
    @Email(message = "* Email doit etre sous le format e.g: **@**.com", regexp = ".+@.+\\..+")
    @NotBlank(message = "* il faut reseinger  votre mail  ")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "EmailUser{" +
                "email='" + email + '\'' +
                '}';
    }
}
