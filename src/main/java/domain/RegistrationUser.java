package domain;


import Annotations.spec.Age;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Arrays;

public class RegistrationUser {

    @NotBlank(message = "{domain.RegistrationUser.name}")
    // @NotEmpty(message =  " le champs ne doit pas etre empty ")
    private String name;

    @NotBlank(message = "{domain.RegistrationUser.username}")
    private String username;

    @NotBlank(message = "{domain.RegistrationUser.password}")
    private String password;

    private String country;
    private String[] hobbies;
    private String gender = "Male";
    @Age(min = 15, max = 60)
    private Integer age;
    @Valid
    private Communication communication;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Communication getCommunication() {
        return communication;
    }

    public void setCommunication(Communication communication) {
        this.communication = communication;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getHobbies() {
        return hobbies;
    }

    public void setHobbies(String[] hobbies) {
        this.hobbies = hobbies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "RegistrationUser{" +
                "name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", country='" + country + '\'' +
                ", hobbies=" + Arrays.toString(hobbies) +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", communication=" + communication +
                '}';
    }


}
