package domain;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


public class Phone {

    @Size(min = 1, max = 3, message = "* le code  pays doit etre entre 2 au 3 chiffres ")
    @NotBlank(message = "* le code pays ne doit pas etre null ou 0 ")
    private String coodepays = "";

    @Size(min = 9, max = 9, message = "* le numero doit contenir 9 chiffres ")
    @NotBlank(message = "* le numero ne doit pas etre null ou 0 ")
    private String numero = "";

    public String getCoodepays() {
        return coodepays;
    }

    public void setCoodepays(String coodepays) {
        this.coodepays = coodepays;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return coodepays + "-" + numero;
    }
}
