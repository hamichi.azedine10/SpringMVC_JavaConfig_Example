package domain;


import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;

public class InfoUser {
    @NotBlank(message = "* le User name ne doit pas etre vide  vide ")
    private String yourname = "insert your name";
    @NotBlank(message = "* le crushname ne doit pas etre vide")
    private String crushname = "insert your crushname";

    private  String result ;

    @AssertTrue(message = "* Vous devez accepter les conditions licence  de ce formulaire ")
    private boolean usecondition;

    public String getYourname() {
        return yourname;
    }

    public void setYourname(String yourname) {
        this.yourname = yourname;
    }

    public String getCrushname() {
        return crushname;
    }

    public void setCrushname(String crushname) {
        this.crushname = crushname;
    }

    public boolean isUsecondition() {
        return usecondition;
    }

    public void setUsecondition(boolean usecondition) {
        this.usecondition = usecondition;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "InfoUser{" +
                "yourname='" + yourname + '\'' +
                ", crushname='" + crushname + '\'' +
                ", result='" + result + '\'' +
                ", usecondition=" + usecondition +
                '}';
    }
}
