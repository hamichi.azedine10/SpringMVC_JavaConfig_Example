package domain;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class Communication {
    @Email(message = "* Email doit etre sous le format e.g: **@**.com", regexp = ".+@.+\\..+")
    @NotBlank(message = "* il faut enregistrer votre mail  ")
    private String email;

    @Valid
    private Phone phone;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Communication{" +
                "email=" + email +
                ", phone=" + phone +
                '}';
    }
}
