package Formatters;

import Controllers.LCappController;
import domain.Phone;
import org.apache.log4j.Logger;
import org.springframework.format.Formatter;

import java.util.Locale;

public class PhoneFormatter implements Formatter<Phone> {
    private final static Logger logger = Logger.getLogger(LCappController.class);

    //Parse a text String to produce a T.
    @Override
    public Phone parse(String phonenumber, Locale locale) {
        //coder la logique pour formatter lles donnée recus
        logger.debug("inside  parse method ");
        logger.debug("phonenumber " + phonenumber);
        Phone phone = new Phone();
        if (phonenumber.contains("-")) {
            logger.debug("phonenumber contient le caracter -");
            String[] phonenumberarray = phonenumber.split("-");
            if (phonenumber.startsWith("-")) {
                logger.debug("rajouter un code pays par defaut ");
                phone.setCoodepays("213");
            } else {
                phone.setCoodepays(phonenumberarray[0]);
            }
            phone.setNumero(phonenumberarray[1]);
        } else if (!phonenumber.contains("-") && phonenumber.length() > 3) {
            logger.debug("inside  elseif  method ");
            String[] phonenumberarray = phonenumber.split("-");
            phone.setCoodepays("213");
            phone.setCoodepays(phonenumberarray[0]);
        } else {
            logger.error("numero  inccorect");
            new RuntimeException("numero  inccorect");
        }
        return phone;
    }

    //  Print the object of type T for display.
    @Override
    public String print(Phone phone, Locale locale) {
        logger.debug("inside  print method ");
        return phone.getCoodepays() + "-" + phone.getNumero();
    }
}
