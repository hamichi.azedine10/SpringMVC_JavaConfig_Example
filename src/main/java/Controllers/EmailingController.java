package Controllers;

import Services.LCappMailSender;
import domain.Connectioninfo;
import domain.EmailUser;
import domain.InfoUser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;


@Controller
@RequestMapping("/emailing")
public class EmailingController {
    private final static Logger logger = Logger.getLogger(EmailingController.class);

    @Autowired
     private LCappMailSender mailSender;


    @GetMapping("/envoiereport")
    public String validationEmail(@ModelAttribute("EmailUserDTO") EmailUser emailUser , @SessionAttribute("connexioninfo") Connectioninfo  connectioninfo, Model model /**, @CookieValue("LCappController.username")String username, Model model*/) {
        logger.debug("inside validationEmail |  mapping.GET :  /emailing/envoiereport");

          model.addAttribute("username", connectioninfo.getName() );
        // logger.debug("model  "+model);*/
        return "sendemail-page";
    }

    @PostMapping("/sendEmail")
    public String envoieEmail(@Valid @ModelAttribute("EmailUserDTO") EmailUser emailUser, BindingResult result, RedirectAttributes redirectAttributes, @SessionAttribute("connexioninfo") Connectioninfo  connectioninfo, @SessionAttribute("resultinfouser") String resultinfouser) {
        logger.debug("inside validationEmail |  mapping.POST :  /emailing/sendEmail");
        String text = "Salut "+connectioninfo.getName()+" , le resultat de prediction est "+resultinfouser;
        if (result.hasErrors()) {
            logger.error("les erreurs de validation ");
            for (ObjectError error : result.getAllErrors()
            ) {
                logger.error(error);
            }
            return "sendemail-page";
        }
        try {
            mailSender.sendmail(emailUser.getEmail(), "LC application prediction result ", text);

            redirectAttributes.addFlashAttribute("emailUser", emailUser);
            redirectAttributes.addFlashAttribute("username ", connectioninfo.getName());
            return "redirect:/SMVCExample/emailing/sendEmailsuccessful";
        }catch (Exception e){
            logger.error("erreur d'envoie  " + e.getMessage() );
            return "sendemail-page";
        }

    }

    @GetMapping("/sendEmailsuccessful")
    public String envoiemailsucces(/**@CookieValue("LCappController.username")String username, Model model*/) {
        logger.debug("inside envoiemailsucces |  mapping.GET :  /emailing/sendEmailsuccessful");

        /** model.addAttribute("username", username);*/
        return "emailingsucces-page";
    }

}
