package Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/testController/")
public class testController {
    public testController() {
    }

    /*  private static Logger logger = Logger.getLogger(testController.class);

      public static void main(String[] args) {
          logger.debug("msg de debogage");
          logger.info("msg d'information");
          logger.warn("msg d'avertissement");
          logger.error("msg d'erreur");
          logger.fatal("msg d'erreur fatale");
      }
  */
    @GetMapping("/test")
    String hellepage() {
        return "hellopage";
    }

}
