package Controllers;

import domain.RegistrationUser;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import propertyEditors.NamePropertyEditor;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/register")
public class ResgistrationController {
    private List<String> liste;

    public ResgistrationController() throws IOException {
        Path path = Paths.get("/home/azedine/eclipse-workspace/SpringMVC_JavaConfig_Example/src/main/resources/countries.csv");
        liste = new ArrayList<>();
        liste = Files.readAllLines(path);
    }

    private final static Logger logger = Logger.getLogger(ResgistrationController.class);
    @GetMapping("/registrationpage")
    public String registeration(@ModelAttribute("RegisterUserInfo") RegistrationUser registrationUser, Model model) {
        logger.debug("inside registeration method ");
         model.addAttribute("liste", liste);
//        Communication communication = new Communication();
//        Phone phone = new Phone();
//        communication.setPhone(phone);
//        registrationUser.setCommunication(communication);
        return "registrationpage";
    }

    @PostMapping("/RegistrationUser")
    public String RegistrationUser(@Valid @ModelAttribute("RegisterUserInfo") RegistrationUser registrationUser, BindingResult errors, Model model) {
        logger.debug("inside  RegistrationUser methode ");
        logger.debug("name : |" + registrationUser.getName() + "|");
        model.addAttribute("liste", liste);
        registrationUser.getCommunication().setPhone(registrationUser.getCommunication().getPhone());
        if (errors.hasErrors()) {
            for (ObjectError objectError : errors.getAllErrors()
            ) {
                logger.error(objectError);
            }
            return "registrationpage";
        } else {
            return "registrationsuccesspage";
        }
    }
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        logger.debug("inside initBinder methode ");
        // todo : ignorer le traitment de ma liste par NamePropertyEditor
        //property editors
        StringTrimmerEditor editor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, editor);

        //convertir le nom  en majuscule
        NamePropertyEditor namePropertyEditor = new NamePropertyEditor();
        dataBinder.registerCustomEditor(String.class, namePropertyEditor);


    }
}
