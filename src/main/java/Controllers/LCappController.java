package Controllers;

import Services.LCalculating;
import domain.Connectioninfo;
import domain.InfoUser;
import domain.RegistrationUser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import propertyEditors.NamePropertyEditor;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * data binding : avec Medel et @RequestParam   17h15
 * data binding : remplacer @RequestParam  par une DTO   17h41
 * utliser ModelAttribute a la place du Model
 */

@Controller
@SessionAttributes({"connexioninfo","resultinfouser"})
public class LCappController {
    private final static Logger logger = Logger.getLogger(LCappController.class);


    @Autowired
    LCalculating lCalculating ;

    @GetMapping("/Singin")
    public String connexion(Model model, Connectioninfo connectioninfo /**@ModelAttribute("connexioninfo") Connectioninfo connectioninfo   , HttpServletRequest request*/) {
        model.addAttribute("connexioninfo", connectioninfo);
        logger.debug("inside connexion method");
        /**logger.info("aproche 1 :HttpServletRequest  | lire tous les coockies existants  ");
         Cookie[] cookies = request.getCookies();
         if (cookies != null) {
         for (Cookie cookie : cookies
         ) {
         logger.debug(cookie.getName());
         }
         if (cookies.length >= 1) {
         for (Cookie cookie : cookies) {
         if (cookie.getName().equals("LCappController.username")) {
         connectioninfo.setName(cookie.getValue());
         logger.debug("connectioninfo " + connectioninfo);
         }
         }
         }
         }*/
        return "connexionpage";
    }

    @PostMapping("/Singin")
    public String connexionstate(Model model, @Valid Connectioninfo connectioninfo, BindingResult result
                                 /**@Valid @ModelAttribute("connexioninfo") Connectioninfo connectioninfo, BindingResult result, HttpServletRequest request , HttpServletResponse response*/) {
        model.addAttribute("connexioninfo", connectioninfo);

        // send error result to jsp manually
        model.addAttribute(BindingResult.MODEL_KEY_PREFIX+"connexioninfo", result);
        if (result.hasErrors()) {
            logger.error("les erreurs de validation ");
            for (ObjectError error : result.getAllErrors()
            ) {
                logger.error(error);
            }
            return "connexionpage";
        }
        /**logger.debug("creer le Cookie ");
         // creer un  Cookie pour sauvegarder le nom de user
         Cookie cookie = new Cookie("LCappController.username", connectioninfo.getName());
         // durer survie de cookie
         cookie.setMaxAge(60 * 60 * 24);
         // ajouter le cookie à la reponse
         response.addCookie(cookie);*/

        /**logger.debug("creer la session ");
         HttpSession session = request.getSession();
         session.setAttribute("username",connectioninfo.getName());
         logger.debug("durée de vie de la session,  ici  1 heure  ");
         session.setMaxInactiveInterval(60);
         */

        return "redirect:/SMVCExample/calculating";
    }

    @GetMapping("/calculating")
    public String calculating(@ModelAttribute("user") InfoUser infoUser, Model model/**, HttpSession session, @CookieValue("LCappController.username" )String username*/) {
        logger.debug("inside calculating method ");

        /**logger.info("aproche 2 :@CookieValue  | lire le coockie existant   ");
         infoUser.setYourname(username);
         infoUser.setYourname((String) session.getAttribute("username"));*/
          Connectioninfo connectioninfo = (Connectioninfo ) model.getAttribute("connexioninfo");
          infoUser.setYourname(connectioninfo.getName());
        return "calculatepage";
    }

    @PostMapping("/process-homepage")
    public String showResultPage(@Valid @ModelAttribute("user") InfoUser infoUser, BindingResult result, RedirectAttributes redirectAttributes, Model model) {
        logger.debug("inside showResultPage method ");
        if (result.hasErrors()) {
            return "calculatepage";
        }
        infoUser.setResult(lCalculating.calculate(infoUser.getYourname(),infoUser.getCrushname()));
        model.addAttribute("resultinfouser", infoUser.getResult());
        redirectAttributes.addFlashAttribute("infoUser", infoUser);
        return "redirect:/SMVCExample/calculatesucces";
    }


    // todo : to fix :  2em appel de cette URL ne permet pas de recuperer le infouser et le resultat attendue (suite au refresh)
    @GetMapping("/calculatesucces")
    public String showhomepage() {
        logger.debug("inside showhomepage method ");
        return "result-page";
    }


    /**
     * la mathde annotée par @InitBinder sera invoquée avant chaque requete (url) passée sur ce controlleur
     * exemple : avant d'excuter la methode qui a le mappiing @PostMapping("/RegistrationUser")  ,qui est la methode
     * RegistrationUser, on execute d'abord  void  initBinder () , c'est la meme chose pour le rest des mehodes
     * 20201219_1438 : ajouter {@link WebDataBinder} comme @param
     */
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        logger.debug("inside initBinder methode ");
        //dataBinder.setDisallowedFields("name");

        //property editors
        StringTrimmerEditor editor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, editor);

        //convertir le nom  en majuscule
        NamePropertyEditor namePropertyEditor = new NamePropertyEditor();
        dataBinder.registerCustomEditor(String.class, namePropertyEditor);
    }

}


