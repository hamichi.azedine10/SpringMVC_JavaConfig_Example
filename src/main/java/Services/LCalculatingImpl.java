package Services;

import domain.ConstantFlame;
import org.springframework.stereotype.Service;

@Service
public class LCalculatingImpl  implements LCalculating{

    @Override
    public String calculate(String username, String crushname) {
        // somme des deux noms --> recuperer la taille de la somme des chaines de characteres
        int sum = (username+crushname).toCharArray().length;

        // taille di model FLMAE
        int devider = ConstantFlame.FLAME.length();
        // recuperer le le modulo
        int rem = sum%devider;

        return  getresultat(rem) ;
    }

    private String getresultat(int rem) {
        String resulat;
        switch (rem){
            case 0:
               resulat= ConstantFlame.F_MEANINH;
                break;
            case (1):
                resulat= ConstantFlame.L_MEANING;
                break;
            case(2):
                resulat= ConstantFlame.A_MEANING;
                break;
            case(3):
                resulat= ConstantFlame.M_MEANING;
                break;
            case (4):
                resulat= ConstantFlame.E_MEANING;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + rem);
        }
        return resulat;
    }
}
