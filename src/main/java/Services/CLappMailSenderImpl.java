package Services;

import Configuration.RootApplicationContext;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class CLappMailSenderImpl implements LCappMailSender {
    private  Logger logger= Logger.getLogger(CLappMailSenderImpl.class);
    @Autowired
    JavaMailSender javaMailSender ;

    @Autowired
    SimpleMailMessage simpleMailMessage;
    @Override
    public void sendmail(String mailTo, String subject, String mailBody) {
        logger.debug("inside sendmail method ");
        simpleMailMessage.setTo(mailTo);
        simpleMailMessage.setText(mailBody);
        javaMailSender.send(simpleMailMessage);
    }
}
