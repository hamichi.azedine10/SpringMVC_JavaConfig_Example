package propertyEditors;

import org.apache.log4j.Logger;

import java.beans.PropertyEditorSupport;

public class NamePropertyEditor extends PropertyEditorSupport {
    private final static Logger logger = Logger.getLogger(NamePropertyEditor.class);

    @Override
    public void setAsText(String s) throws IllegalArgumentException {
        logger.trace("inside NamePropertyEditor.setAsText method ");
        logger.trace(s);
        String s1 = s.toUpperCase();
        setValue(s1);
    }

    /*  @Override public String getAsText() {
    Phone phone = new Phone();
    phone.setCoodepays("999");
    phone.setNumero("754577426");
    return  phone.toString();
    }*/
}
