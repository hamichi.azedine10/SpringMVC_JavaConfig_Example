package Configuration;

import Formatters.PhoneFormatter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Properties;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"Controllers"})
@PropertySource("classpath:Configuration.properties")
//implimenter WebMvcConfigurer pour ajouter le PhoneFormatter + message de validation (messages.properties)
public class ServletApplicationContext implements WebMvcConfigurer {
    private static Logger logger = Logger.getLogger(ServletApplicationContext.class);

    @Value("${context.interviewer.prefix}")
    private String viewResolverPrefix;
    @Value("${context.interviewer.suffix}")
    private String viewResolverSuffix;

    /**
     * ce bean est ajouté suite a  l'ajout de messageresource qui empeche
     *
     * @PropertySource("classpath:Configuration.properties") de fonctionner cerrcetement
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        logger.trace("inside propertySourcesPlaceholderConfigurer bean ");
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public InternalResourceViewResolver internalResourceViewResolver() {
        logger.trace("inside internalResourceViewResolver bean ");
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix(viewResolverPrefix);
        viewResolver.setSuffix(viewResolverSuffix);
        return viewResolver;
    }

    /**
     * choisir le  ou les fichiers properties pour les messages de validation
     */
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames();
        messageSource.setBasename("messages");
        for (String s : messageSource.getBasenameSet()) {
            logger.debug(s);
        }
        return messageSource;
    }




    @Bean(name = "validator")
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setValidationMessageSource(messageSource());
        return localValidatorFactoryBean;
    }

    @Override
    public Validator getValidator() {
        return validator();
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        logger.debug("jouter PhoneFormatter au regitre des formatteurs ");
        registry.addFormatter(new PhoneFormatter());
    }

}
