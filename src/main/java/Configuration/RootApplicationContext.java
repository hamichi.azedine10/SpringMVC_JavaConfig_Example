package Configuration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"Daos", "Services", "domain"})
@PropertySource("classpath:email.properties")
public class RootApplicationContext {
    private static Logger logger = Logger.getLogger(ServletApplicationContext.class);

    // lire le fichier properties via l'objet Environnment
    @Autowired
    Environment environment ;

    // creer un mail sender qui envoie les simple mail
    @Bean
    public JavaMailSender getEmailSender() {
        logger.trace("inside getEmailSender bean ");

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(environment.getProperty("mail.Host"));
        mailSender.setPort(Integer.parseInt(environment.getProperty("mail.Port")));
        mailSender.setUsername(environment.getProperty("mail.Username"));
        mailSender.setPassword(environment.getProperty("mail.Password"));

        mailSender.setJavaMailProperties(getMailProperties());

        return mailSender;
    }

    private Properties getMailProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        return props;
    }

    @Bean
    public SimpleMailMessage simpleMailMessage(){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("hamichiazedine@gmail.com");
        return simpleMailMessage;
    }

}
