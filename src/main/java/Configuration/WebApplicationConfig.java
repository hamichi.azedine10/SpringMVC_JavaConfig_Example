package Configuration;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebApplicationConfig extends AbstractAnnotationConfigDispatcherServletInitializer {
    private static Logger logger = Logger.getLogger(WebApplicationConfig.class);

    /**
     * Specify {@code @Configuration} and/or {@code @Component} classes for the
     * {@linkplain #createRootApplicationContext() root application context}.
     *
     * @return the configuration for the root application context, or {@code null}
     * if creation and registration of a root context is not desired
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        logger.trace("inside WebApplicationConfig.getRootConfigClasses method  ");
        logger.trace("specifier la ou les classe(s) de configuration pour le root context  ");
        return new Class[]{RootApplicationContext.class};
    }

    /**
     * Specify {@code @Configuration} and/or {@code @Component} classes for the
     * {@linkplain #createServletApplicationContext() Servlet application context}.
     *
     * @return the configuration for the Servlet application context, or
     * {@code null} if all configuration is specified through root config classes.
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        logger.trace("inside WebApplicationConfig.getServletConfigClasses method  ");
        logger.trace("specifier la ou les  classe(s) de configuration pour le dispatcher servlet  context  ");
        return new Class[]{ServletApplicationContext.class};
    }

    /**
     * Specify the servlet mapping(s) for the {@code DispatcherServlet} &mdash;
     * for example {@code "/"}, {@code "/app"}, etc.
     * <p>
     * //@see #registerDispatcherServlet(ServletContext)
     */
    @Override
    protected String[] getServletMappings() {
        logger.trace("inside WebApplicationConfig.getServletMappings method  ");
        return new String[]{"/SMVCExample/*"};
    }
}