package Annotations.spec;

import Annotations.impl.AgeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target({METHOD, FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = AgeValidator.class)
public @interface Age {
    String message() default "{annotations.spec.Age.message}";

    int min() default 18;

    int max() default 65;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}
