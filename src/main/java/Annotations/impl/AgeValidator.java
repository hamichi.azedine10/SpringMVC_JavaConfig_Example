package Annotations.impl;

import Annotations.spec.Age;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AgeValidator implements ConstraintValidator<Age, Integer> {
    int minimum;
    int maximum;

    public void initialize(Age age) {

       /*
        initialize variables
       cette methode est execututée  une seule fois
       */
        minimum = age.min();
        maximum = age.max();
    }

    public boolean isValid(Integer obj, ConstraintValidatorContext context) {
        if (obj == null) {
            return false;
        }
        return obj >= minimum && obj <= maximum;
    }
}
